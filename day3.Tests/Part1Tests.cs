using day3;
namespace day3.Tests;

public class SolvePart1_Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void SolvePart1_ValidInput_44()
    {
        var input = new List<string>{
            "0100",
            "0100",
            "0100",
            "0100",
        };
        var result = day3.Program.SolvePart1(input);
        Assert.AreEqual(44, result);
    }

    [Test]
    public void SolvePart1_ValidInput_60()
    {
        var input = new List<string>{
            "0001",
            "0001",
            "0001",
            "0101",
            "0101",
            "0100",
            "0100",
            "0100",
        };
        var result = day3.Program.SolvePart1(input);
        Assert.AreEqual(50, result);
    }

    [Test]
    public void SolvePart1_ValidInput_0()
    {
        var input = new List<string>{
            "0001",
            "0000",
            "0000",
            "0101",
            "0000",
            "0100",
            "0100",
            "0100",
        };
        var result = day3.Program.SolvePart1(input);
        Assert.AreEqual(0, result);
    }
}