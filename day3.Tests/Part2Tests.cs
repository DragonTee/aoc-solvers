using day3;
namespace day3.Tests;

public class SolvePart2_Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void SolvePart2_ValidInput_16()
    {
        var input = new List<string>{
            "0100",
            "0001",
            "1000",
            "0010",
        };
        var result = day3.Program.SolvePart2(input);
        Assert.AreEqual(16, result);
    }

    [Test]
    public void SolvePart2_ValidInput_0()
    {
        var input = new List<string>{
            "1000",
            "0000",
        };
        var result = day3.Program.SolvePart1(input);
        Assert.AreEqual(0, result);
    }
}