﻿class Day1Task {
static int Task1(){
    string fileName = "./input";
    string[] depthMeasurement = File.ReadAllLines(fileName);
    int numberOfMeasurements = depthMeasurement.Count();
    int numberOfIncreases = 0;
    for (int i = 1; i < numberOfMeasurements;i++){
        if (Convert.ToInt32(depthMeasurement[i]) > Convert.ToInt32(depthMeasurement[i-1])) ++numberOfIncreases;
    }
    return numberOfIncreases;
}

static int Task2(){
    string fileName = "./input2";
    string[] depthMeasurement = File.ReadAllLines(fileName);
    int numberOfMeasurements = depthMeasurement.Count();
    int numberOfIncreases = 0;
    int currentSum = 0;
    int previousSum = Convert.ToInt32(depthMeasurement[0]) + Convert.ToInt32(depthMeasurement[1]) + Convert.ToInt32(depthMeasurement[2]);  
    for (int i = 3; i < numberOfMeasurements;i++){
        currentSum = Convert.ToInt32(depthMeasurement[i]) + Convert.ToInt32(depthMeasurement[i-1]) + Convert.ToInt32(depthMeasurement[i-2]);
        if (currentSum > previousSum) ++numberOfIncreases;
        previousSum = currentSum;
    }
    return numberOfIncreases;
}
static void Main(string[] args){
    Console.WriteLine("Результат первой части: "+Task1());
    Console.WriteLine("Результат второй части: "+Task2());
}
}