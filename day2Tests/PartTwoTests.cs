using Day2;
namespace day2Tests;

[TestFixture]
public class PartTwoTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        var input = new List<string>{
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2"
        };
        var result = Program.PartTwoFromList(input);
        Assert.That(result, Is.EqualTo(900));
    }


    [Test]
    public void Test2()
    {
        var input = new List<string>{
            "forward 5",
            "down 5",
            "forward 5",
        };
        var result = Program.PartTwoFromList(input);
        Assert.That(result, Is.EqualTo(250));
    }
}