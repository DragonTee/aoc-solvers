﻿namespace day3
{
    public class Program
    {
        public static int SolvePart1(List<string> lines)
        {
            var linesCount = lines.Count();
            var lineLength = lines[0].Length;
            var bitCounter = new int[lineLength];
            foreach (var line in lines)
                for (int i = 0; i < line.Length; i++)
                    if (line[i] == '1')
                        bitCounter[i]++;
            int resultGamma = 0;
            int resultEpsilon = 0;
            for (int i = 0; i < lineLength; i++)
            {
                int newBitGamma = bitCounter[i] > (linesCount - bitCounter[i]) ? 1 : 0;
                int newBitEpsilon = bitCounter[i] <= (linesCount - bitCounter[i]) ? 1 : 0;
                resultGamma = (resultGamma << 1) + newBitGamma;
                resultEpsilon = (resultEpsilon << 1) + newBitEpsilon;
            }
            return resultGamma * resultEpsilon;
        }

        public static int SolvePart1(string inputFilePath)
        {
            var linesList = new List<string>();
            var lines = File.ReadLines(inputFilePath);
            foreach (var line in lines)
            {
                linesList.Add(line.Trim());
            }
            return SolvePart1(linesList);
        }

        static List<string> Part2IterateColumn(List<string> lines, int column, bool findMostBits)
        {
            int bitCounter = 0;
            for (int i = 0; i < lines.Count(); i++)
            {
                if (lines[i][column] == '1')
                    bitCounter++;
            }
            bool selectOnes = (bitCounter >= lines.Count - bitCounter);
            if (!findMostBits)
                selectOnes = (bitCounter < lines.Count - bitCounter);
            var newList = lines.Where(x => x[column] == (selectOnes ? '1' : '0')).ToList();
            return newList;
        }

        public static int SolvePart2(List<string> lines)
        {
            var lineLength = lines[0].Length;
            var linesOxygen = lines;
            var linesScrubber = lines;
            int column = 0;
            while (linesOxygen.Count > 1 && column < lineLength)
            {
                linesOxygen = Part2IterateColumn(linesOxygen, column, true);
                column++;
            }
            column = 0;
            while (linesScrubber.Count > 1 && column < lineLength)
            {;
                linesScrubber = Part2IterateColumn(linesScrubber, column, false);
                column++;
            }
            int resultOxygen = 0;
            int resultScrubber = 0;
            var bitCounterOxygen = new int[lineLength];
            var bitCounterScrubber = new int[lineLength];
            foreach (var line in linesOxygen)
                for (int i = 0; i < lineLength; i++)
                    if (line[i] == '1')
                        bitCounterOxygen[i]++;
            foreach (var line in linesScrubber)
                for (int i = 0; i < lineLength; i++)
                    if (line[i] == '1')
                        bitCounterScrubber[i]++;
            for (int i = 0; i < lineLength; i++)
            {
                int newBitOxygen = bitCounterOxygen[i];
                int newBitScrubber = bitCounterScrubber[i];
                resultOxygen = (resultOxygen << 1) + newBitOxygen;
                resultScrubber = (resultScrubber << 1) + newBitScrubber;
            }
            return resultOxygen * resultScrubber;
        }

        public static int SolvePart2(string inputFilePath)
        {
            var linesList = new List<string>();
            var lines = File.ReadLines(inputFilePath);
            foreach (var line in lines)
            {
                linesList.Add(line.Trim());
            }
            return SolvePart2(linesList);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Solution for part 1: {0}", SolvePart1("input"));
            Console.WriteLine("Solution for part 2: {0}", SolvePart2("input"));
        }
    }
}