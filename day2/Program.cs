﻿namespace Day2
{

    public class Program{    

        public static int PartOneFromFile(string fileName)
        {
            string[] allCommands = File.ReadAllLines(fileName);
            int horizontalPos = 0;
            int depth = 0;

            foreach (string currentCommand in allCommands)
            {
                string[] splitedCommand = currentCommand.Split(' ');
                switch(splitedCommand[0])
                {
                    case "forward":
                    {
                        horizontalPos+=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                    case "down":
                    {
                        depth+=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                    case "up":
                    {
                        depth-=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                }
            }
            int result = horizontalPos*depth;
            return result;
        }

        public static int PartOneFromList(IList<string> commands)
        {
            int horizontalPos = 0;
            int depth = 0;

            foreach (string currentCommand in commands)
            {
                string[] splitedCommand = currentCommand.Split(' ');
                switch(splitedCommand[0])
                {
                    case "forward":
                    {
                        horizontalPos+=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                    case "down":
                    {
                        depth+=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                    case "up":
                    {
                        depth-=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                }
            }
            int result = horizontalPos*depth;
            return result;
        }


        public static int PartTwoFromFile(string fileName)
        {
            string[] allCommands = File.ReadAllLines(fileName);
            int horizontalPos = 0;
            int depth = 0;
            int aim = 0;

            foreach (string currentCommand in allCommands)
            {
                string[] splitedCommand = currentCommand.Split(' ');
                switch(splitedCommand[0])
                {
                    case "forward":
                    {
                        int x = Convert.ToInt32(splitedCommand[1]);
                        horizontalPos+=x;
                        depth+=aim*x;
                        continue;
                    }
                    case "down":
                    {
                        aim+=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                    case "up":
                    {
                        aim-=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                }
            }
            int result = horizontalPos*depth;
            return result;
        }

        public static int PartTwoFromList(IList<string> commands)
        {
            int horizontalPos = 0;
            int depth = 0;
            int aim = 0;

            foreach (string currentCommand in commands)
            {
                string[] splitedCommand = currentCommand.Split(' ');
                switch(splitedCommand[0])
                {
                    case "forward":
                    {
                        int x = Convert.ToInt32(splitedCommand[1]);
                        horizontalPos+=x;
                        depth+=aim*x;
                        continue;
                    }
                    case "down":
                    {
                        aim+=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                    case "up":
                    {
                        aim-=Convert.ToInt32(splitedCommand[1]);
                        continue;
                    }
                }
            }
            int result = horizontalPos*depth;
            return result;
        }

        static void Main(string[] args)
        {
            PartTwoFromFile("./input");
        }
    }
}